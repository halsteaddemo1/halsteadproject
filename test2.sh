#!/bin/bash

echo "Enter the path to install code"
read -r -p "Enter path: " path
echo "$path"

if [ -d "$path/halsteadproject" ]; then
    echo "Directory already exists"
else
    echo "Enter GitLab project ID: "
    read -r -p "Project ID: " projectId
    echo "Enter GitLab access token: "
    read -r -p "Access Token: " accessToken

    gitUrl="https://gitlab.com/api/v4/projects/$projectId/repository/archive?private_token=$accessToken"
    curl -L --header "PRIVATE-TOKEN: $accessToken" "$gitUrl" -o "$path/archive.tar.gz"

    mkdir "$path/halsteadproject"
    tar -xvf "$path/archive.tar.gz" -C "$path/halsteadproject" --strip-components=1
    rm "$path/archive.tar.gz"
fi

cd "$path/halsteadproject/"

g++ halstead31.cpp -o hal

echo "Enter the name of the output file (.csv)"
read -r -p "Enter outfile path: " outfile
echo "$outfile"

echo "Enter the path of the folder containing all files"
read -r -p "Enter Folder path: " filespath
echo "$filespath"

echo "Processing CPP files..."
while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        echo "Processing file: $file"
        dirname=$(dirname "$file")
        ./hal "$outfile" "$filespath"
    else
        echo "File not found: $file"
    fi
done < <(find "$filespath" -type f -name "*.cpp" -print0)

echo "CSV file generated successfully."

exit 0

